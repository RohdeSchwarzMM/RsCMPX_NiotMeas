NiotMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:BAND

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:BAND



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.NiotMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval.rst
	Configure_NiotMeas_Prach.rst
	Configure_NiotMeas_RfSettings.rst