MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<instance>:MEValuation:FSYRange
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:DMODe
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:MMODe
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:CPRefix
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:CBANdwidth
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:PLCid
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:DSS
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCSPacing
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:NPFormat
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:NREPetitions
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:NRUNits
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:NSLots

.. code-block:: python

	CONFigure:NIOT:MEASurement<instance>:MEValuation:FSYRange
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:DMODe
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:MMODe
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:CPRefix
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:CBANdwidth
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:PLCid
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:DSS
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCSPacing
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:NPFormat
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:NREPetitions
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:NRUNits
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:NSLots



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_Limit.rst
	Configure_NiotMeas_MultiEval_ListPy.rst
	Configure_NiotMeas_MultiEval_Modulation.rst
	Configure_NiotMeas_MultiEval_Pdynamics.rst
	Configure_NiotMeas_MultiEval_Result.rst
	Configure_NiotMeas_MultiEval_Scount.rst
	Configure_NiotMeas_MultiEval_Spectrum.rst
	Configure_NiotMeas_MultiEval_Subcarrier.rst