Limit<Limit>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.configure.niotMeas.multiEval.limit.repcap_limit_get()
	driver.configure.niotMeas.multiEval.limit.repcap_limit_set(repcap.Limit.Nr1)





.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_Limit_Aclr.rst
	Configure_NiotMeas_MultiEval_Limit_EvMagnitude.rst
	Configure_NiotMeas_MultiEval_Limit_FreqError.rst
	Configure_NiotMeas_MultiEval_Limit_Ibe.rst
	Configure_NiotMeas_MultiEval_Limit_IqOffset.rst
	Configure_NiotMeas_MultiEval_Limit_Merror.rst
	Configure_NiotMeas_MultiEval_Limit_Pdynamics.rst
	Configure_NiotMeas_MultiEval_Limit_Perror.rst
	Configure_NiotMeas_MultiEval_Limit_SeMask.rst