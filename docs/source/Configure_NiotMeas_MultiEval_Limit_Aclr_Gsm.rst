Gsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:ACLR:GSM

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:ACLR:GSM



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.Aclr.Gsm.GsmCls
	:members:
	:undoc-members:
	:noindex: