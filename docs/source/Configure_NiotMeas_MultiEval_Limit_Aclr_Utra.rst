Utra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:ACLR:UTRA

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:ACLR:UTRA



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.Aclr.Utra.UtraCls
	:members:
	:undoc-members:
	:noindex: