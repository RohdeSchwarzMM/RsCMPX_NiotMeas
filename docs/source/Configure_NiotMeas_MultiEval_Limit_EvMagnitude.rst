EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:EVMagnitude



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: