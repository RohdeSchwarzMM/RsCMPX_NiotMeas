FreqError
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:FERRor

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:FERRor



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: