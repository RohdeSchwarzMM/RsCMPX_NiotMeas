Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:MERRor

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:MERRor



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: