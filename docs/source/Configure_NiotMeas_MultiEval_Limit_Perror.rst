Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:PERRor

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:PERRor



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: