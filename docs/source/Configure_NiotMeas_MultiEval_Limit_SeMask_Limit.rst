Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Limit.SeMask.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex: