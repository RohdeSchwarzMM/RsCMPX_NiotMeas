ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:OSINdex
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:CMODe
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:NCONnections
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:OSINdex
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:CMODe
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:NCONnections
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_ListPy_Lrange.rst
	Configure_NiotMeas_MultiEval_ListPy_Segment.rst
	Configure_NiotMeas_MultiEval_ListPy_SingleCmw.rst