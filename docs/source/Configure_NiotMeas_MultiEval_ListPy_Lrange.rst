Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:LRANge

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:LRANge



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: