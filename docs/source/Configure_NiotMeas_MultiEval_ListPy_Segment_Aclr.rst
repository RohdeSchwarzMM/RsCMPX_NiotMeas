Aclr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Segment.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex: