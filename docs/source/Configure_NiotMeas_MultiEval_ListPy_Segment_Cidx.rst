Cidx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CIDX

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CIDX



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Segment.Cidx.CidxCls
	:members:
	:undoc-members:
	:noindex: