Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: