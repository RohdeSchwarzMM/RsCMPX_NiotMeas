SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Segment.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: