Setup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SETup

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SETup



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Segment.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex: