SingleCmw
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.Segment.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.listPy.segment.singleCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_ListPy_Segment_SingleCmw_Connector.rst