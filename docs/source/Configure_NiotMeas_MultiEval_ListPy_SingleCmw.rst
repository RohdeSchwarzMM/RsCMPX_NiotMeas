SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: