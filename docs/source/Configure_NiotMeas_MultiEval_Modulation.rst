Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:MODulation:MSCHeme

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:MODulation:MSCHeme



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_Modulation_EePeriods.rst