Npusch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:NPUSch:LEADing
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:NPUSch:LAGGing

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:NPUSch:LEADing
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:NPUSch:LAGGing



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Modulation.EePeriods.Npusch.NpuschCls
	:members:
	:undoc-members:
	:noindex: