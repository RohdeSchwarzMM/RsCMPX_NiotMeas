Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult[:ALL]
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:IEMissions
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:TXM
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:IQ
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:SEMask
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:ACLR
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:PMONitor
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:PDYNamics

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult[:ALL]
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:IEMissions
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:TXM
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:IQ
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:SEMask
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:ACLR
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:PMONitor
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:RESult:PDYNamics



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: