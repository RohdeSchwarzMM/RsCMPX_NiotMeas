Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:POWer

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:POWer



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.scount.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_Scount_Spectrum.rst