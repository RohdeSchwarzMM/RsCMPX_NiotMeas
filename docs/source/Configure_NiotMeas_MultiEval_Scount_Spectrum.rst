Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Scount.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: