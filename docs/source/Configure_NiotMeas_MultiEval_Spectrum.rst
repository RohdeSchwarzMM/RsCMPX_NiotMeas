Spectrum
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.multiEval.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_MultiEval_Spectrum_Aclr.rst
	Configure_NiotMeas_MultiEval_Spectrum_SeMask.rst