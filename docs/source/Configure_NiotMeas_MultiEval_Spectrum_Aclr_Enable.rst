Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Spectrum.Aclr.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: