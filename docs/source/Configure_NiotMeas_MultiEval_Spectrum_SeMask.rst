SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:OBWMode

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:OBWMode



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Spectrum.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: