Subcarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:MEValuation:SUBCarrier

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:MEValuation:SUBCarrier



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.MultiEval.Subcarrier.SubcarrierCls
	:members:
	:undoc-members:
	:noindex: