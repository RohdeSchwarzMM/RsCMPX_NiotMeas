Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:TOUT
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:REPetition
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:SCONdition
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:MOEXception
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:PFORmat
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:NOPReambles
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:POPReambles

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:TOUT
	CONFigure:NIOT:MEASurement<Instance>:PRACh:REPetition
	CONFigure:NIOT:MEASurement<Instance>:PRACh:SCONdition
	CONFigure:NIOT:MEASurement<Instance>:PRACh:MOEXception
	CONFigure:NIOT:MEASurement<Instance>:PRACh:PFORmat
	CONFigure:NIOT:MEASurement<Instance>:PRACh:NOPReambles
	CONFigure:NIOT:MEASurement<Instance>:PRACh:POPReambles



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_Prach_Limit.rst
	Configure_NiotMeas_Prach_Modulation.rst
	Configure_NiotMeas_Prach_Result.rst
	Configure_NiotMeas_Prach_Scount.rst