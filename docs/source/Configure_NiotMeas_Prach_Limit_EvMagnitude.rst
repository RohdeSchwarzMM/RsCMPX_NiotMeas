EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:LIMit:EVMagnitude



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: