Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:LIMit:MERRor

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:LIMit:MERRor



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: