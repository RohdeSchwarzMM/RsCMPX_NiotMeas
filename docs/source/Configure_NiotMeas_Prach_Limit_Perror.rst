Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:LIMit:PERRor

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:LIMit:PERRor



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: