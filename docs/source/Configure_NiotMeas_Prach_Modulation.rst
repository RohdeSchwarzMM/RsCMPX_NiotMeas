Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:MODulation:EWPosition

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:MODulation:EWPosition



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_Prach_Modulation_EwLength.rst