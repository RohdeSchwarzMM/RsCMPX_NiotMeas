EwLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:MODulation:EWLength

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:MODulation:EWLength



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Modulation.EwLength.EwLengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.niotMeas.prach.modulation.ewLength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NiotMeas_Prach_Modulation_EwLength_Pformat.rst