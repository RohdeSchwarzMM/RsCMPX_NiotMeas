Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult[:ALL]
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:EVPReamble
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:MERRor
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:PERRor
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:IQ
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:PDYNamics
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:PVPReamble
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:TXM

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult[:ALL]
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:EVPReamble
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:MERRor
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:PERRor
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:IQ
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:PDYNamics
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:PVPReamble
	CONFigure:NIOT:MEASurement<Instance>:PRACh:RESult:TXM



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: