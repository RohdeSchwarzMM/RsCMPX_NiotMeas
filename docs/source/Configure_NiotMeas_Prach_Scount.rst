Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:SCOunt:MODulation
	single: CONFigure:NIOT:MEASurement<Instance>:PRACh:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:PRACh:SCOunt:MODulation
	CONFigure:NIOT:MEASurement<Instance>:PRACh:SCOunt:PDYNamics



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.Prach.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: