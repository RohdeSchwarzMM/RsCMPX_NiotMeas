RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NIOT:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:NIOT:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:NIOT:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:NIOT:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:NIOT:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:NIOT:MEASurement<Instance>:RFSettings:MLOFfset

.. code-block:: python

	CONFigure:NIOT:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:NIOT:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:NIOT:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:NIOT:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:NIOT:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:NIOT:MEASurement<Instance>:RFSettings:MLOFfset



.. autoclass:: RsCMPX_NiotMeas.Implementations.Configure.NiotMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: