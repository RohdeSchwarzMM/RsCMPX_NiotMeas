MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NIOT:MEASurement<Instance>:MEValuation
	single: STOP:NIOT:MEASurement<Instance>:MEValuation
	single: ABORt:NIOT:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:NIOT:MEASurement<Instance>:MEValuation
	STOP:NIOT:MEASurement<Instance>:MEValuation
	ABORt:NIOT:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Aclr.rst
	NiotMeas_MultiEval_EvMagnitude.rst
	NiotMeas_MultiEval_InbandEmission.rst
	NiotMeas_MultiEval_ListPy.rst
	NiotMeas_MultiEval_Merror.rst
	NiotMeas_MultiEval_Modulation.rst
	NiotMeas_MultiEval_Pdynamics.rst
	NiotMeas_MultiEval_Perror.rst
	NiotMeas_MultiEval_SeMask.rst
	NiotMeas_MultiEval_State.rst
	NiotMeas_MultiEval_Trace.rst