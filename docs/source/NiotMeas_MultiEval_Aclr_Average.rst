Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:ACLR:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:ACLR:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:ACLR:AVERage
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:ACLR:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: