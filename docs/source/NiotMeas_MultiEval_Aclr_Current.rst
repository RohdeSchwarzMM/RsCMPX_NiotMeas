Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:ACLR:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:ACLR:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:ACLR:CURRent
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:ACLR:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: