Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: