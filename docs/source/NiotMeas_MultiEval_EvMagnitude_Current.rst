Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: