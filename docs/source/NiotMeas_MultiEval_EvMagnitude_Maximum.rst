Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: