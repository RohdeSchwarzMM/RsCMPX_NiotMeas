Peak
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.evMagnitude.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_EvMagnitude_Peak_Average.rst
	NiotMeas_MultiEval_EvMagnitude_Peak_Current.rst
	NiotMeas_MultiEval_EvMagnitude_Peak_Maximum.rst