Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: