Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: