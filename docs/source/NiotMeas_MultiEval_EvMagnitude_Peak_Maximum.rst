Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.EvMagnitude.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: