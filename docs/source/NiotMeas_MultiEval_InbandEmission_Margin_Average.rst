Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:AVERage

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.InbandEmission.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: