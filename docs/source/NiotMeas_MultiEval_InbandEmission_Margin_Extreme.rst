Extreme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:EXTReme

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:EXTReme



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.InbandEmission.Margin.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.inbandEmission.margin.extreme.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_InbandEmission_Margin_Extreme_ScIndex.rst