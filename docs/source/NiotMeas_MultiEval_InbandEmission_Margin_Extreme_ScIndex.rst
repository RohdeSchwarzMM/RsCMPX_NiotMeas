ScIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:EXTReme:SCINdex

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:EXTReme:SCINdex



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.InbandEmission.Margin.Extreme.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: