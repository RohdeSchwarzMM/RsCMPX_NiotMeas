StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:SDEViation

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:IEMission:MARGin:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.InbandEmission.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: