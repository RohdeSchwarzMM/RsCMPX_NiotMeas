Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2000
	rc = driver.niotMeas.multiEval.listPy.segment.repcap_segment_get()
	driver.niotMeas.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_ListPy_Segment_Aclr.rst
	NiotMeas_MultiEval_ListPy_Segment_InbandEmission.rst
	NiotMeas_MultiEval_ListPy_Segment_Modulation.rst
	NiotMeas_MultiEval_ListPy_Segment_SeMask.rst