Margin
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.InbandEmission.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.listPy.segment.inbandEmission.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_ListPy_Segment_InbandEmission_Margin_Average.rst
	NiotMeas_MultiEval_ListPy_Segment_InbandEmission_Margin_Current.rst
	NiotMeas_MultiEval_ListPy_Segment_InbandEmission_Margin_Extreme.rst
	NiotMeas_MultiEval_ListPy_Segment_InbandEmission_Margin_StandardDev.rst