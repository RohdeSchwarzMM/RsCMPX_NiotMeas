ScIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:MARGin:CURRent:SCINdex

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:MARGin:CURRent:SCINdex



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.InbandEmission.Margin.Current.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: