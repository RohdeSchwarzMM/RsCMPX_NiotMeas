StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:MARGin:SDEViation

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:MARGin:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.InbandEmission.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: