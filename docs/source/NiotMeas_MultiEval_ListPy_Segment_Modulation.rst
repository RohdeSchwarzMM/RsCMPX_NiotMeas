Modulation
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.listPy.segment.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_ListPy_Segment_Modulation_Average.rst
	NiotMeas_MultiEval_ListPy_Segment_Modulation_Current.rst
	NiotMeas_MultiEval_ListPy_Segment_Modulation_Extreme.rst
	NiotMeas_MultiEval_ListPy_Segment_Modulation_StandardDev.rst