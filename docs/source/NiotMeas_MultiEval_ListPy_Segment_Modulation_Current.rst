Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:CURRent

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: