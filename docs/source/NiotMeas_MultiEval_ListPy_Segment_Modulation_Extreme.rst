Extreme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:EXTReme

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:EXTReme



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: