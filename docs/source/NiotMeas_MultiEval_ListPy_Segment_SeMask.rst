SeMask
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.listPy.segment.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_ListPy_Segment_SeMask_Average.rst
	NiotMeas_MultiEval_ListPy_Segment_SeMask_Current.rst
	NiotMeas_MultiEval_ListPy_Segment_SeMask_Extreme.rst
	NiotMeas_MultiEval_ListPy_Segment_SeMask_Margin.rst
	NiotMeas_MultiEval_ListPy_Segment_SeMask_StandardDev.rst