All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:ALL

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:ALL



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.SeMask.Margin.All.AllCls
	:members:
	:undoc-members:
	:noindex: