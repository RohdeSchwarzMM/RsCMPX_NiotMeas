Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:AVERage:NEGativ

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:AVERage:NEGativ



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Segment.SeMask.Margin.Average.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: