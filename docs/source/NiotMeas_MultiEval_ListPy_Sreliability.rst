Sreliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SRELiability

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:LIST:SRELiability



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.ListPy.Sreliability.SreliabilityCls
	:members:
	:undoc-members:
	:noindex: