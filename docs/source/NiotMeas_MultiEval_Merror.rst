Merror
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Merror_Average.rst
	NiotMeas_MultiEval_Merror_Current.rst
	NiotMeas_MultiEval_Merror_Maximum.rst