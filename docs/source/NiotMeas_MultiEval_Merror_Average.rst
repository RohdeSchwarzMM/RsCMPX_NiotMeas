Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MERRor:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MERRor:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MERRor:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MERRor:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: