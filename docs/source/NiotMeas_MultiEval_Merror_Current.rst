Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MERRor:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MERRor:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MERRor:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MERRor:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: