Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MERRor:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MERRor:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: