Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:MODulation:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:AVERage
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:MODulation:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: