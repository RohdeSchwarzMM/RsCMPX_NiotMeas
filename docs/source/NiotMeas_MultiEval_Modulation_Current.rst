Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:MODulation:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:CURRent
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:MODulation:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: