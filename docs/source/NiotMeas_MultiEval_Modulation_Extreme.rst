Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:MODulation:EXTReme

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:MODulation:EXTReme



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: