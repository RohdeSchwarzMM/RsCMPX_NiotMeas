StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:SDEViation

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	FETCh:NIOT:MEASurement<Instance>:MEValuation:MODulation:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: