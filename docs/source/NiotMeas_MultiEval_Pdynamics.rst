Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Pdynamics_Average.rst
	NiotMeas_MultiEval_Pdynamics_Current.rst
	NiotMeas_MultiEval_Pdynamics_Maximum.rst
	NiotMeas_MultiEval_Pdynamics_Minimum.rst
	NiotMeas_MultiEval_Pdynamics_StandardDev.rst