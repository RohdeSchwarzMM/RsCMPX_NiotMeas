Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: