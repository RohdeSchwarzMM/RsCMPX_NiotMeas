Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: