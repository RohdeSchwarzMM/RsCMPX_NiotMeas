Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: