Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MINimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:MINimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: