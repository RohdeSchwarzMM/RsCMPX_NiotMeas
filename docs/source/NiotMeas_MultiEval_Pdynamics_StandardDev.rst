StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: