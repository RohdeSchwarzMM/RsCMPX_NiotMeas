Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PERRor:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PERRor:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PERRor:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PERRor:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: