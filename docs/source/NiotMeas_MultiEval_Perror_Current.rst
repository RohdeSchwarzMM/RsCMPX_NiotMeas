Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PERRor:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PERRor:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PERRor:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PERRor:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: