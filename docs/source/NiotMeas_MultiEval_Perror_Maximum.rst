Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:PERRor:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:PERRor:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: