SeMask
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_SeMask_Average.rst
	NiotMeas_MultiEval_SeMask_Current.rst
	NiotMeas_MultiEval_SeMask_Extreme.rst
	NiotMeas_MultiEval_SeMask_Margin.rst
	NiotMeas_MultiEval_SeMask_StandardDev.rst