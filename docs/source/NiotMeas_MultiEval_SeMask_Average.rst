Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:SEMask:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:AVERage
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:SEMask:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: