Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:SEMask:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:CURRent
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:SEMask:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: