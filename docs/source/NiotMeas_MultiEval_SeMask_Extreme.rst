Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: CALCulate:NIOT:MEASurement<Instance>:MEValuation:SEMask:EXTReme

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	CALCulate:NIOT:MEASurement<Instance>:MEValuation:SEMask:EXTReme



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: