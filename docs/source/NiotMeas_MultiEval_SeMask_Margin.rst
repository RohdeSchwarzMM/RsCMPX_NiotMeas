Margin
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.seMask.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_SeMask_Margin_All.rst
	NiotMeas_MultiEval_SeMask_Margin_Average.rst
	NiotMeas_MultiEval_SeMask_Margin_Current.rst
	NiotMeas_MultiEval_SeMask_Margin_Minimum.rst