Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:NEGativ

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:NEGativ



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.Average.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: