Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:POSitiv

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:POSitiv



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.Average.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: