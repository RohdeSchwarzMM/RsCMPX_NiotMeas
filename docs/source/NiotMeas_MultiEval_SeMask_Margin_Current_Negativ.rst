Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:NEGativ

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:NEGativ



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.Current.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: