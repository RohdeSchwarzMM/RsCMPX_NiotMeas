Minimum
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.seMask.margin.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_SeMask_Margin_Minimum_Negativ.rst
	NiotMeas_MultiEval_SeMask_Margin_Minimum_Positiv.rst