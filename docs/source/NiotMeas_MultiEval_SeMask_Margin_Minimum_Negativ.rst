Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:NEGativ

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:NEGativ



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.Minimum.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: