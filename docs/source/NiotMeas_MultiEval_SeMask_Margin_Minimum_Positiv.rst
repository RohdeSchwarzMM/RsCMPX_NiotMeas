Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.Margin.Minimum.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: