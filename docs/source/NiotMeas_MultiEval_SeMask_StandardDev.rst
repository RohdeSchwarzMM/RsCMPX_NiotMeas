StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:SDEViation

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	FETCh:NIOT:MEASurement<Instance>:MEValuation:SEMask:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.SeMask.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: