State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:STATe

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:STATe



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_State_All.rst