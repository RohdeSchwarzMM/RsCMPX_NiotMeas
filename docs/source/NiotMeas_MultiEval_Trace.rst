Trace
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Trace_Aclr.rst
	NiotMeas_MultiEval_Trace_EvmSymbol.rst
	NiotMeas_MultiEval_Trace_Iemissions.rst
	NiotMeas_MultiEval_Trace_Iq.rst
	NiotMeas_MultiEval_Trace_Pdynamics.rst
	NiotMeas_MultiEval_Trace_Pmonitor.rst
	NiotMeas_MultiEval_Trace_SeMask.rst