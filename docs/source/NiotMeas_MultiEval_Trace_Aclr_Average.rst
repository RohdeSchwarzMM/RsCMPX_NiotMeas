Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: