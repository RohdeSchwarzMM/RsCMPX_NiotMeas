Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: