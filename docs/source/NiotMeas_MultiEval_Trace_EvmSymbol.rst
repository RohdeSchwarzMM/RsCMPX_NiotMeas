EvmSymbol
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.trace.evmSymbol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Trace_EvmSymbol_Average.rst
	NiotMeas_MultiEval_Trace_EvmSymbol_Current.rst
	NiotMeas_MultiEval_Trace_EvmSymbol_Maximum.rst