Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: