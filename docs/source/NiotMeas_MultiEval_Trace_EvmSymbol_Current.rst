Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: