Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: