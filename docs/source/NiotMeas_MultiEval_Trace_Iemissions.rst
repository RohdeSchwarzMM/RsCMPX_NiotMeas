Iemissions
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:IEMissions
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:IEMissions

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:IEMissions
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:IEMissions



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Iemissions.IemissionsCls
	:members:
	:undoc-members:
	:noindex: