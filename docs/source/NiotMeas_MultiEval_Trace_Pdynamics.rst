Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Trace_Pdynamics_Average.rst
	NiotMeas_MultiEval_Trace_Pdynamics_Current.rst
	NiotMeas_MultiEval_Trace_Pdynamics_Maximum.rst
	NiotMeas_MultiEval_Trace_Pdynamics_Post.rst