Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: