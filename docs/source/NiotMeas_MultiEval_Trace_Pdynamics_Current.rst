Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: