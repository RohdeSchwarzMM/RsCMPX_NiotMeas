Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: