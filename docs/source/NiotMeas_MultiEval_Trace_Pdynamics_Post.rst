Post
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Post.PostCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.multiEval.trace.pdynamics.post.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_MultiEval_Trace_Pdynamics_Post_Average.rst
	NiotMeas_MultiEval_Trace_Pdynamics_Post_Current.rst
	NiotMeas_MultiEval_Trace_Pdynamics_Post_Maximum.rst