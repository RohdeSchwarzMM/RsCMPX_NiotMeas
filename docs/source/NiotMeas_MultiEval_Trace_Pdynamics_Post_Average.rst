Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Post.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: