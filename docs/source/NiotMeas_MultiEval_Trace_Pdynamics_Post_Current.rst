Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Post.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: