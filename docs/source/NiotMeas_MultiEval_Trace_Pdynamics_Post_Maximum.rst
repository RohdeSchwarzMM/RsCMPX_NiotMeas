Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:POST:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pdynamics.Post.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: