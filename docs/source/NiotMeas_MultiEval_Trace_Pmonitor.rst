Pmonitor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PMONitor
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PMONitor

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:PMONitor
	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:PMONitor



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex: