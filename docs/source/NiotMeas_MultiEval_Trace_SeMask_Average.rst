Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:AVERage
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.SeMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: