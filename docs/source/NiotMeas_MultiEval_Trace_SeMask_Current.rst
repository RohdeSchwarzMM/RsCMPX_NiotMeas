Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:CURRent
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: