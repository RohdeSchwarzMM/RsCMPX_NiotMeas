Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:MAXimum
	FETCh:NIOT:MEASurement<Instance>:MEValuation:TRACe:SEMask:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.MultiEval.Trace.SeMask.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: