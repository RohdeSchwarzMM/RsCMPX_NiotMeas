Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NIOT:MEASurement<Instance>:PRACh
	single: STOP:NIOT:MEASurement<Instance>:PRACh
	single: ABORt:NIOT:MEASurement<Instance>:PRACh

.. code-block:: python

	INITiate:NIOT:MEASurement<Instance>:PRACh
	STOP:NIOT:MEASurement<Instance>:PRACh
	ABORt:NIOT:MEASurement<Instance>:PRACh



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Modulation.rst
	NiotMeas_Prach_Pdynamics.rst
	NiotMeas_Prach_State.rst
	NiotMeas_Prach_Trace.rst