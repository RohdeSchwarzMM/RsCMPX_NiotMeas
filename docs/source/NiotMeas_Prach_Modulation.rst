Modulation
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Modulation_Average.rst
	NiotMeas_Prach_Modulation_Current.rst
	NiotMeas_Prach_Modulation_Extreme.rst
	NiotMeas_Prach_Modulation_Preamble.rst
	NiotMeas_Prach_Modulation_ScsGroup.rst
	NiotMeas_Prach_Modulation_StandardDev.rst