Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: CALCulate:NIOT:MEASurement<Instance>:PRACh:MODulation:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:MODulation:AVERage
	FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:AVERage
	CALCulate:NIOT:MEASurement<Instance>:PRACh:MODulation:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: