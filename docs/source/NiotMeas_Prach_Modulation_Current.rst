Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: CALCulate:NIOT:MEASurement<Instance>:PRACh:MODulation:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:MODulation:CURRent
	FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:CURRent
	CALCulate:NIOT:MEASurement<Instance>:PRACh:MODulation:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: