Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: CALCulate:NIOT:MEASurement<Instance>:PRACh:MODulation:EXTReme

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:MODulation:EXTReme
	FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:EXTReme
	CALCulate:NIOT:MEASurement<Instance>:PRACh:MODulation:EXTReme



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: