ScsGroup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:SCSGroup

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:SCSGroup



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.ScsGroup.ScsGroupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.modulation.scsGroup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Modulation_ScsGroup_Preamble.rst