Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.niotMeas.prach.modulation.scsGroup.preamble.repcap_preamble_get()
	driver.niotMeas.prach.modulation.scsGroup.preamble.repcap_preamble_set(repcap.Preamble.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:SCSGroup:PREamble<Number>

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:SCSGroup:PREamble<Number>



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.ScsGroup.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.modulation.scsGroup.preamble.clone()