StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:MODulation:SDEViation
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:SDEViation

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:MODulation:SDEViation
	FETCh:NIOT:MEASurement<Instance>:PRACh:MODulation:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: