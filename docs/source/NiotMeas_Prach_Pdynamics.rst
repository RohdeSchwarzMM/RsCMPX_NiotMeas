Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Pdynamics_Average.rst
	NiotMeas_Prach_Pdynamics_Current.rst
	NiotMeas_Prach_Pdynamics_Maximum.rst
	NiotMeas_Prach_Pdynamics_Minimum.rst
	NiotMeas_Prach_Pdynamics_StandardDev.rst