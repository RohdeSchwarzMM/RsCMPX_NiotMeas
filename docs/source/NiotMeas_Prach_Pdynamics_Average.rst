Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: CALCulate:NIOT:MEASurement<Instance>:PRACh:PDYNamics:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	CALCulate:NIOT:MEASurement<Instance>:PRACh:PDYNamics:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: