Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: CALCulate:NIOT:MEASurement<Instance>:PRACh:PDYNamics:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	CALCulate:NIOT:MEASurement<Instance>:PRACh:PDYNamics:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: