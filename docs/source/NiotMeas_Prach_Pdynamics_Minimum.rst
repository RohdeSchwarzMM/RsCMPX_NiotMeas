Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: CALCulate:NIOT:MEASurement<Instance>:PRACh:PDYNamics:MINimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	CALCulate:NIOT:MEASurement<Instance>:PRACh:PDYNamics:MINimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: