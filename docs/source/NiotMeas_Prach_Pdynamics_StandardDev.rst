StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:SDEViation

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	FETCh:NIOT:MEASurement<Instance>:PRACh:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: