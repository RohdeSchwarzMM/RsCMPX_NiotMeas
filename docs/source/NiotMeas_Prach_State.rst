State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:STATe

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:PRACh:STATe



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_State_All.rst