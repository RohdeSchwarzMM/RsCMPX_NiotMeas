EvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVPReamble

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVPReamble



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.EvPreamble.EvPreambleCls
	:members:
	:undoc-members:
	:noindex: