Evm
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.trace.evm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Trace_Evm_Average.rst
	NiotMeas_Prach_Trace_Evm_Current.rst
	NiotMeas_Prach_Trace_Evm_Maximum.rst