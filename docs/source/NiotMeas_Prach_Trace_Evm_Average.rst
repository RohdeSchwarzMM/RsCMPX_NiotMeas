Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Evm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: