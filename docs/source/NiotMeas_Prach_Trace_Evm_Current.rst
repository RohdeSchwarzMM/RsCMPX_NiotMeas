Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: