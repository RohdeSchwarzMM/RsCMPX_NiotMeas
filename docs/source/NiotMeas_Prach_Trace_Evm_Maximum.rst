Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Evm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: