Iq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:IQ

.. code-block:: python

	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:IQ



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Iq.IqCls
	:members:
	:undoc-members:
	:noindex: