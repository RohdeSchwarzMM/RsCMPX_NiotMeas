Merror
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.trace.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Trace_Merror_Average.rst
	NiotMeas_Prach_Trace_Merror_Current.rst
	NiotMeas_Prach_Trace_Merror_Maximum.rst