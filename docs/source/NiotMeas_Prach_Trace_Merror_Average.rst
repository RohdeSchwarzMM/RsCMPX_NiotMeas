Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: