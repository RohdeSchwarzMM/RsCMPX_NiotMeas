Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: