Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: