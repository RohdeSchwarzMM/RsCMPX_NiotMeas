Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Trace_Pdynamics_Average.rst
	NiotMeas_Prach_Trace_Pdynamics_Current.rst
	NiotMeas_Prach_Trace_Pdynamics_Maximum.rst