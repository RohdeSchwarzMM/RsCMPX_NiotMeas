Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: