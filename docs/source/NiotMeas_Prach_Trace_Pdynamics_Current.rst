Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: