Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: