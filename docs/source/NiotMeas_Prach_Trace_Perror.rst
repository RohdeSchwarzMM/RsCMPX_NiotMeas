Perror
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.niotMeas.prach.trace.perror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NiotMeas_Prach_Trace_Perror_Average.rst
	NiotMeas_Prach_Trace_Perror_Current.rst
	NiotMeas_Prach_Trace_Perror_Maximum.rst