Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: