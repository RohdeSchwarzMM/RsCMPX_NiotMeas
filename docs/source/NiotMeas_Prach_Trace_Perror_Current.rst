Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: