Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: