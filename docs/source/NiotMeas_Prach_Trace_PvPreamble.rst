PvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	single: FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PVPReamble

.. code-block:: python

	READ:NIOT:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	FETCh:NIOT:MEASurement<Instance>:PRACh:TRACe:PVPReamble



.. autoclass:: RsCMPX_NiotMeas.Implementations.NiotMeas.Prach.Trace.PvPreamble.PvPreambleCls
	:members:
	:undoc-members:
	:noindex: