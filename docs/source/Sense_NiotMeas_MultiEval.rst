MultiEval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NIOT:MEASurement<Instance>:MEValuation:NSRunits

.. code-block:: python

	SENSe:NIOT:MEASurement<Instance>:MEValuation:NSRunits



.. autoclass:: RsCMPX_NiotMeas.Implementations.Sense.NiotMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex: