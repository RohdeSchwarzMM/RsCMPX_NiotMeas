NiotMeas
----------------------------------------





.. autoclass:: RsCMPX_NiotMeas.Implementations.Trigger.NiotMeas.NiotMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.niotMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NiotMeas_MultiEval.rst
	Trigger_NiotMeas_Prach.rst