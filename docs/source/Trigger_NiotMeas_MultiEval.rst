MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NIOT:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:NIOT:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:NIOT:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:NIOT:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:NIOT:MEASurement<Instance>:MEValuation:MGAP

.. code-block:: python

	TRIGger:NIOT:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:NIOT:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:NIOT:MEASurement<Instance>:MEValuation:DELay
	TRIGger:NIOT:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:NIOT:MEASurement<Instance>:MEValuation:MGAP



.. autoclass:: RsCMPX_NiotMeas.Implementations.Trigger.NiotMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.niotMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NiotMeas_MultiEval_ListPy.rst