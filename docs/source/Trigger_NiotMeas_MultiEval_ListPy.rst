ListPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NIOT:MEASurement<Instance>:MEValuation:LIST:MODE

.. code-block:: python

	TRIGger:NIOT:MEASurement<Instance>:MEValuation:LIST:MODE



.. autoclass:: RsCMPX_NiotMeas.Implementations.Trigger.NiotMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: