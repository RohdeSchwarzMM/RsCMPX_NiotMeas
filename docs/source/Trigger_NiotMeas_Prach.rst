Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NIOT:MEASurement<Instance>:PRACh:THReshold
	single: TRIGger:NIOT:MEASurement<Instance>:PRACh:SLOPe
	single: TRIGger:NIOT:MEASurement<Instance>:PRACh:TOUT
	single: TRIGger:NIOT:MEASurement<Instance>:PRACh:MGAP

.. code-block:: python

	TRIGger:NIOT:MEASurement<Instance>:PRACh:THReshold
	TRIGger:NIOT:MEASurement<Instance>:PRACh:SLOPe
	TRIGger:NIOT:MEASurement<Instance>:PRACh:TOUT
	TRIGger:NIOT:MEASurement<Instance>:PRACh:MGAP



.. autoclass:: RsCMPX_NiotMeas.Implementations.Trigger.NiotMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex: