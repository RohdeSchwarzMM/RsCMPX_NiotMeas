Enums
=========

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Band.OB1
	# Last value:
	value = enums.Band.UDEF
	# All values (39x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB17 | OB18
	OB19 | OB2 | OB20 | OB21 | OB22 | OB23 | OB24 | OB25
	OB255 | OB256 | OB26 | OB27 | OB28 | OB3 | OB30 | OB31
	OB4 | OB5 | OB65 | OB66 | OB68 | OB7 | OB70 | OB71
	OB72 | OB73 | OB74 | OB8 | OB85 | OB9 | UDEF

ChannelBw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBw.B200
	# All values (1x):
	B200

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

CyclicPrefix
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CyclicPrefix.EXTended
	# All values (2x):
	EXTended | NORMal

LeadLag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeadLag.OFF
	# All values (2x):
	OFF | S1

ListMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListMode.ONCE
	# All values (2x):
	ONCE | SEGMent

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.MELMode
	# All values (2x):
	MELMode | NORMal

Mode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Mode.FDD
	# All values (2x):
	FDD | TDD

ModScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModScheme.BPSK
	# All values (3x):
	BPSK | Q16 | QPSK

NofRepetitions
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NofRepetitions.NR1
	# Last value:
	value = enums.NofRepetitions.NR8
	# All values (12x):
	NR1 | NR128 | NR16 | NR1K | NR2 | NR256 | NR2K | NR32
	NR4 | NR512 | NR64 | NR8

NofRepetitionsList
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NofRepetitionsList.NR1
	# Last value:
	value = enums.NofRepetitionsList.NR8
	# All values (16x):
	NR1 | NR128 | NR1536 | NR16 | NR192 | NR1K | NR2 | NR256
	NR2K | NR32 | NR384 | NR4 | NR512 | NR64 | NR768 | NR8

NofRsrcUnits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NofRsrcUnits.NRU01
	# All values (8x):
	NRU01 | NRU02 | NRU03 | NRU04 | NRU05 | NRU06 | NRU08 | NRU10

NpuschFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NpuschFormat.F1
	# All values (2x):
	F1 | F2

ObwMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ObwMode.BW99
	# All values (2x):
	BW99 | M26

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PeriodPreamble
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeriodPreamble.MS160
	# All values (5x):
	MS160 | MS240 | MS320 | MS40 | MS80

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerFlag.IFPower
	# All values (3x):
	IFPower | OFF | ON

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SubCarrSpacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubCarrSpacing.S15K
	# All values (2x):
	S15K | S3K75

TargetStateA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetStateA.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TimeMask
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeMask.GOO
	# All values (1x):
	GOO

